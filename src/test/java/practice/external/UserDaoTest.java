package practice.external;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class UserDaoTest {

    @Test
    public void test() throws SQLException {
        ApplicationContext applicationContext = new GenericXmlApplicationContext("config.xml");
        UserDao userDao = applicationContext.getBean("userDao", UserDao.class);

        userDao.deleteAll();
        assertEquals(userDao.getCount(), 0);

        User user1 = buildUser("yelimid1", "김예림", "PASSWORD");
        User user2 = buildUser("yelimid2", "김예리미", "password");

        userDao.add(user1);
        userDao.add(user2);
        assertEquals(userDao.getCount(), 2);

        User userget1 = userDao.get(user1.getId());
        assertEquals(userget1.getName(), user1.getName());
        assertEquals(userget1.getPassword(), user1.getPassword());

        User userget2 = userDao.get(user2.getId());
        assertEquals(userget2.getName(), user2.getName());
        assertEquals(userget2.getPassword(), user2.getPassword());
    }

    @Test
    public void getCount() throws SQLException {
        ApplicationContext applicationContext = new GenericXmlApplicationContext("config.xml");

        UserDao userDao = applicationContext.getBean("userDao", UserDao.class);
        User user1 = buildUser("id1", "박상철", "password");
        User user2 = buildUser("id2", "박상철", "password");
        User user3 = buildUser("id3", "박상철", "password");

        userDao.deleteAll();
        assertEquals(userDao.getCount(), 0);

        userDao.add(user1);
        assertEquals(userDao.getCount(), 1);

        userDao.add(user2);
        assertEquals(userDao.getCount(), 2);

        userDao.add(user3);
        assertEquals(userDao.getCount(), 3);
    }

    private User buildUser(String id, String name, String password) {
        User user = new User();
        user.setId(id);
        user.setName(name);
        user.setPassword(password);
        return user;
    }
}
